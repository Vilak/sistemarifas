function Rifa(){
  var url = "";

  this.setUrl = function(base_url){
    url = base_url;
  }

  this.eventos_ = function(){
    eventos();
  }
  this.cargar_rifa_ = function(rifa_id){
    cargar_rifa(rifa_id);
  }

  var eventos = function(){
    $(".boton_ver_boleto").off();
    $(".agregar_pago_boleto").off();
    $(".boton_guardar_pago").off();
    $(".agregar_boleto").off();
    $(".guardar_compra_boleto").off();
    $(".boton_acepta_boleto").off();

    $(".boton_acepta_boleto").click(function(){
      cargar_rifa(localStorage.getItem("rifa_id"));
      $(".container_modal").hide();
    });

    $(".guardar_compra_boleto").click(function(){
      var boleto_num = $(this).attr("id");
      var rifa_id = $(".rifa_id_hid").val();
      var comprador = $(".select_comprador").val();
      var vendedor = $(".select_vendedor").val();
      var payment = $(".cant_pago_inicial").val();
      guarda_compra(boleto_num,rifa_id,comprador,vendedor,payment);
    });

    $(".agregar_boleto").click(function(){
      var rifa_id = $(this).attr("id");
      modal_agregar_boleto(rifa_id);
    });

    $(".boton_guardar_pago").click(function(){
      var idticket = $(this).attr("id");
      var amount_pago = $(this).parents().siblings(".cantidad_pago_input").val();
      console.log(amount_pago);
      $.ajax({
      	method : "POST",
      	async : true,
      	url : url + "index.php/Rifas/guardar_pago_boleto",
      	data: {
          "ticket_id" : idticket,
          "cant_pago" : amount_pago
        },
      }).done(function(data){
        data = JSON.parse(data);
        if(data.pagado == true){
          var c = "";
          c += "<p>El boleto ha sido pagado completamente</p>";
          c += "<div class='botones_modal'>";
          c += "<button class='boton_ok_modal'>Ok</button>";
          c += "</div>";
        }
        else{
          var c = "";
          c += "<p>Pago guardado exitosamente!</p>";
          c += "<p style='text-align:center;'>Cantidad por pagar:</p>";
          c += "<p style='text-align:center;'>" + data.restante + "</p>";
          c += "<div class='botones_modal'>";
          c += "  <button class='boton_ok_modal'>Ok</button>";
          c += "</div>";
        }
        new Modal().modal_generico_(c);
        eventos();
      });
    });

    $(".agregar_pago_boleto").click(function(){
      var bol_id = $(this).attr("id");
      var c = "";
      c += "<p style='text-align:center;'>Agregando pago</p>";
      c += "<p style='text-align:center;'>Boleto #" + bol_id + "</p>";
      c += "<p>Cantidad del pago:</p>";
      c += "<input type='number' class='cantidad_pago_input'>";
      c += "<div class='botones_modal'>";
      c += "  <button class='boton_guardar_pago' id='" + bol_id +"'>Guardar pago</button>";
      c += "  <button class='boton_cerrar_modal'><i class='fas fa-times'></i>Cerrar</button>";
      c += "</div>";
      new Modal().modal_generico_(c);
      eventos();
    });


    $(".boton_ver_boleto").click(function(){
      var boleto_id = $(this).attr("id");
      $.ajax({
      	method : "POST",
      	async : true,
      	url : url + "index.php/Rifas/traer_info_boleto",
      	data: {
          boleto_id : boleto_id
        },
      }).done(function(data){
        data = JSON.parse(data);
        console.log(data);
        var c = "";
        c += "<p style='text-align:center;'>Boleto #" + data.boleto[0].idboleto + "</p>";
        c += "<p style='text-align:center;'>Comprado por: " + data.boleto[0].nombres + " " + data.boleto[0].apellido_paterno + " " + data.boleto[0].apellido_materno + "</p>";
        c += "<table class='tabla_modal'>";
        c += "  <tr>";
        c += "    <th>Pago</th>";
        c += "    <th>Fecha</th>";
        c += "  <tr>";
        for (var i = 0; i < data.pagos.length; i++) {
          c += "  <tr>";
          var fecha_pago = data.pagos[i].fecha_pago.split(" ");
          c += "    <td>" + data.pagos[i].cantidad_pago + "</td>";
          c += "    <td>" + fecha_pago[0] + "</td>";
          c += "  </tr>";
        }
        c += "</table>";
        c += "<div class='botones_modal'>";
        c += "  <button class='agregar_pago_boleto' id='" + data.boleto[0].idboleto + "'><i class='fas fa-plus'></i> Agregar pago</button>";
        c += "  <button class='boton_cerrar_modal'><i class='fas fa-times'></i> Cerrar</button>";
        c += "</div>";
        new Modal().modal_generico_(c);
        eventos();
      });
    });
  }

  var modal_agregar_boleto = function(rifa_id){
    $.ajax({
    	method : "POST",
    	async : true,
    	url : url + "index.php/Rifas/traer_info_compra_boleto",
      data : {
        "rifa_id" : rifa_id
      }
    }).done(function(data){
      data = JSON.parse(data);
      if(data.boletos != 0){
        var nuevo_boleto_id = parseInt(data.boletos[0].num_boleto_rifa) + 1
      }
      else{
        var nuevo_boleto_id = 1;
      }
      if(nuevo_boleto_id <= parseInt(data.rifa[0].cantidad_boletos)){
        var c = "";
        c += "<p>Generando nuevo boleto</p>";
        c += "<p>Numero de boleto:</p>";
        c += "<p>" + nuevo_boleto_id + "</p>";
        c += "<p>Comprador:</p>";
        c += "<select class='select_comprador'>";
        c += "  <option value=''>Seleccione un comprador</option>";
        for(var i = 0; i < data.clientes.length; i++){
          c += "  <option value='" + data.clientes[i].idcliente + "'>" + data.clientes[i].nombres + " " + data.clientes[i].apellido_paterno + " " + data.clientes[i].apellido_materno + "</option>";
        }
        c += "</select>";
        c += "<p>Vendedor:</p>";
        c += "<select class='select_vendedor'>";
        c += "  <option value=''>Seleccione un vendedor</option>";
        for (var j = 0; j < data.vendedores.length; j++) {
          c += "<option value='" + data.vendedores[j].idvendedor + "'>" + data.vendedores[j].nombre_vendedor + " " + data.vendedores[j].ap_vendedor + " " + data.vendedores[j].am_vendedor + "</option>";
        }
        c += "</select>";
        c += "<p>Pago inicial:</p>";
        c += "<input type='number' class='cant_pago_inicial' value=0>";
        c += "<div class='botones_modal'>";
        c += "  <input hidden value='" + rifa_id + "' class='rifa_id_hid'>";
        c += "  <button class='guardar_compra_boleto' id='" + nuevo_boleto_id + "'>Guardar boleto</button>";
        c += "  <button class='boton_cerrar_modal'><i class='fas fa-times'></i> Cerrar</button>";
        c += "</div>";
        new Modal().modal_generico_(c);
        eventos();
      }
      else{
        alert("Ya no quedan boletos disponibles");
      }
    });
  }

  var cargar_rifa = function(rifa_id){
    $.ajax({
    	method : "POST",
    	async : true,
    	url : url + "index.php/Rifas/traer_rifa",
    	data: {
        rifa_id : rifa_id
      },
    }).done(function(data){
      data = JSON.parse(data);
      var id_hidden = data.info_rifa[0].idrifa;
      $(".agregar_boleto").attr("id",id_hidden);
      var fecha_inicio = data.info_rifa[0].fecha_inicio.split(" ");
      var fecha_fin = data.info_rifa[0].fecha_fin.split(" ");
      var c = "";
      c += "<tr>";
      c += "  <td>" + data.info_rifa[0].nombre_rifa + "</td>";
      c += "  <td>" + fecha_inicio[0] + "</td>";
      c += "  <td>" + fecha_fin[0] + "</td>";
      c += "  <td>" + data.info_rifa[0].precio_boleto + "</td>";
      c += "  <td>" + data.info_rifa[0].cantidad_boletos + "</td>";
      c += "  <td>" + data.info_rifa[0].boletos_vendidos + "</td>";
      c += "  <td>" + data.info_rifa[0].total_recaudado + "</td>";
      c += "</tr>";
      var f = "";
      for (var i = 0; i < data.info_boletos.length; i++) {
        f += "<tr>";
        f += "  <td>" + data.info_boletos[i].nombre_vendedor + " " + data.info_boletos[i].ap_vendedor + "</td>";
        f += "  <td>" + data.info_boletos[i].num_boleto_rifa + "</td>";
        f += "  <td>" + data.info_boletos[i].nombres + " " + data.info_boletos[i].apellido_paterno + "</td>";
        f += "  <td>" + data.info_boletos[i].telefono + "</td>";
        f += "  <td>" + data.info_boletos[i].cantidad_pagada + "</td>";
        if(parseInt(data.info_boletos[i].pagado) == 1){
          f += "<td><i class='fas fa-check' style='color:green;'></i></td>";
        }
        else if(parseInt(data.info_boletos[i].pagado) == 0){
          f += "<td><button class='boton_ver_boleto' id='" + data.info_boletos[i].idboleto + "'>Ver boleto</button></td>";
        }
        f += "</tr>";
      }
      $(".tabla_rifa").html(c);
      $(".tabla_boletos").html(f);
      eventos();
    });
  }

  var guarda_compra = function(boleto_num,rifa_id,comprador,vendedor,payment){
    if(boleto_num != "" && rifa_id != "" && comprador != "" && vendedor != "" && payment != ""){
      $.ajax({
      	method : "POST",
      	async : true,
      	url : url + "index.php/Rifas/guarda_boleto",
      	data: {
          "boleto_num" : boleto_num,
          "rifa_id" : rifa_id,
          "comprador" : comprador,
          "vendedor" : vendedor,
          "payment" : payment
        },
      }).done(function(){
        var c = "";
        c += "<p>Boleto creado exitosamente</p>";
        c += "<div class='botones_modal'>";
        c += "  <button class='boton_ok_modal boton_acepta_boleto'>Ok</button>";
        c += "</div>";
        new Modal().modal_generico_(c);
        eventos();
      });
    }
    else{
      alert("Todos los campos son requeridos");
    }
  }
}
