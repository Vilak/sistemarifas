function Anteriores(){
	var url = "";

	this.setUrl = function(base_url){
		url = base_url;
	}

	this.eventos_ = function(){
		eventos();
	}

	var eventos = function(){

	}

	this.llenar_anteriores_ = function(){
		llenar_anteriores();
	}

	var llenar_anteriores = function(){
		$.ajax({
			method : 'POST',
			async : true,
			url : url + 'index.php/Anteriores/traer_rifas_anteriores'
		}).done(function(data){
			data = JSON.parse(data);
			var c = "";
			if(data != ""){
				for(var i = 0; i < data.length; i++){
					var fecha_i = data[i].fecha_inicio.split(" ");
					var fecha_f = data[i].fecha_fin.split(" ");
					c += "<tr>";
					c += "	<td>" + data[i].nombre_rifa + "</td>";
					c += "	<td>" + fecha_i[0] + "</td>";
					c += "	<td>" + fecha_f[0] + "</td>";
					c += "	<td>" + data[i].cantidad_boletos + "</td>";
					c += "	<td>" + data[i].tickets_tot + "</td>";
					c += "	<td>" + data[i].recaudado + "</td>";
					if(data[i].ganador){
						c += "	<td>" + data[i].ganador + "</td>";
					}
					else{
						c += "	<td>No hay ganador definido</td>";
					}
					c += "	<td><button class='boton_ver_terminada' id='" + data[i].idrifa + "'>Ver</button></td>";
					c += "</tr>";
				}

			}
			else{
				alert("No hay rifas anteriores");
				c += "<p style='text-align:center;'>Ninguna rifa anterior</p>";
			}
			$(".tabla_rifas_ant").html(c);
		});
	}

}
