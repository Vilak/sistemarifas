function Modal(){

  this.modal_generico_ = function(texto){
    modal_generico(texto);
  }

  var eventos = function(){
    $(".boton_ok_modal").off();
    $(".boton_cerrar_modal").off();

    $(".boton_cerrar_modal").click(function(){
      $(".container_modal").hide();
    });

    $(".boton_ok_modal").click(function(){
      $(".container_modal").hide();
    });
  }

  var modal_generico = function(texto){
    $(".modal_texto").html("");
    $(".modal_titulo").html("");
    $(".modal_texto").html(texto);
    $(".container_modal").css({"display":"flex"});
    eventos();
  }

}
