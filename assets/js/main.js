function Main(){
  var url = "";
  var header_scroll = false;

  this.setUrl = function(url_){
    url = url_;
  }

  this.eventos_ = function(){
    eventos();
  }

  var eventos = function(){
    $(".rifas_anterior").off();
    $(".rifas_ongoing").off();
    $(".nueva_rifa").off();
    $(".guardar_rifa").off();
    $(".llevar_rifa").off();
    $(".floating_settings").off();
    $(".boton_guardar_usuario").off();

    $(".boton_guardar_usuario").click(function(){
      var tipo = $(".select_tipo_user").val();
      var nombres = $(".names").val();
      var ap_p = $(".ap_paterno").val();
      var ap_m = $(".ap_materno").val();
      var email = $(".email").val();
      var phone = $(".phone_number").val();
      $.ajax({
      	method : "POST",
      	async : true,
      	url : url + "index.php/welcome/guarda_usuario",
      	data: {
          "tipo" : tipo,
          "nombres" : nombres,
          "ap_p" : ap_p,
          "ap_m" : ap_m,
          "email" : email,
          "phone" : phone
        },
      }).done(function(data){
        data = JSON.parse(data);
        if(data){
          alert("Usuario guardado exitosamente");
          $(".container_modal").hide();
        }
        else{
          alert("Ocurrio un error inesperado, por favor intenta de nuevo");
        }
      });
    });

    $(".floating_settings").click(function(){
      var c = "";
      c += "<p>Tipo de usuario:</p>";
      c += "<select class='select_tipo_user'>";
      c += "  <option value=''>Selecciona el tipo de usuario</option>";
      c += "  <option value='cliente'>Cliente</option>";
      c += "  <option value='vendedor'>Vendedor</option>";
      c += "</select>";
      c += "<p>Nombres:</p>";
      c += "<input type='text' class='names'>";
      c += "<p>Apellido paterno:</p>";
      c += "<input type='text' class='ap_paterno'>";
      c += "<p>Apellido materno:</p>";
      c += "<input type='text' class='ap_materno'>";
      c += "<p>Correo electrónico:</p>";
      c += "<input type='text' class='email'>";
      c += "<p>Teléfono:</p>";
      c += "<input type='number' class='phone_number'>";
      c += "<div class='botones_modal'>";
      c += "  <button class='boton_guardar_usuario'><i class='fas fa-plus'></i> Guardar</button>";
      c += "  <button class='boton_cerrar_modal'>Cerrar</button>";
      c += "</div>";
      new Modal().modal_generico_(c);
      eventos();
    });

    $(".llevar_rifa").click(function(){
      var rifa_id = $(this).attr("id");
      localStorage.setItem("rifa_id",rifa_id);
      window.location.href = url + "index.php/Rifas";
    });

    $(".guardar_rifa").click(function(){
      var nombre_rifa = $(this).parents().siblings(".nombre_rifa").val();
      var cant_boletos = $(this).parents().siblings(".cant_tickets").val();
      var ticket_price = $(this).parents().siblings(".precio_boleto").val();
      var fecha_inicio = $(this).parents().siblings(".fecha_inicio").val();
      var fecha_fin = $(this).parents().siblings(".fecha_fin").val();
      if(nombre_rifa != "" && cant_boletos != "" && ticket_price != "" && fecha_inicio != "" && fecha_fin){
        guardar_rifa_nueva(nombre_rifa,cant_boletos,ticket_price,fecha_inicio,fecha_fin);
      }
      else{
        alert("Todos los campos son requeridos");
      }
    });

    $(".rifas_ongoing").click(function(){
      get_current_raffle();
    });

    $(".rifas_anterior").click(function(){
      get_pre_raffles();
    });

    $(".nueva_rifa").click(function(){
      new_raffle();
    });

    $(window).scroll(function(){
      scroll_header();
    });

    $("#datepicker").datepicker({
      dateFormat : "yy-mm-dd"
    });
    $("#datepicker1").datepicker({
      dateFormat : "yy-mm-dd"
    });
  }

  var get_current_raffle = function(){
    window.location.href = url;
  }

  var get_pre_raffles = function(){
    window.location.href = url + "index.php/Anteriores";
  }

  var new_raffle = function(){
    var c = "";
    c += "<div>";
    c += "  <p>Nombre de la rifa:</p>";
    c += "  <input type='text' class='nombre_rifa'>";
    c += "  <p>Cantidad de boletos</p>";
    c += "  <input type='number' class='cant_tickets'>";
    c += "  <p>Precio por boleto</p>";
    c += "  <input type='number' class='precio_boleto'>";
    c += "  <p>Fecha de inicio</p>";
    c += "  <input type='text' class='fecha_inicio' id='datepicker'>";
    c += "  <p>Fecha de conclucion</p>";
    c += "  <input type='text' class='fecha_fin' id='datepicker1'>";
    c += "  <div class='botones_modal'>";
    c += "    <button class='guardar_rifa'>Crear rifa</button>";
    c += "    <button class='boton_cerrar_modal'>Cerrar</button>";
    c += "  </div>";
    c += "</div>";
    new Modal().modal_generico_(c);
    eventos();
  }

  var scroll_header = function(){
    if($(this).scrollTop() > 70){
      header_scrolled = true;
      if(header_scrolled){
        $("header").addClass("scrolled_header");
      }
    }
    else{
      header_scrolled = false;
      if(!header_scrolled){
        $("header").removeClass("scrolled_header");
      }
    }
  }

  var guardar_rifa_nueva = function(nombre_rifa,cant_boletos,ticket_price,fecha_inicio,fecha_fin){
    $.ajax({
    	method : "POST",
    	async : true,
    	url : url + "index.php/welcome/guardar_rifa",
    	data: {
        "nombre" : nombre_rifa,
        "tickets" : cant_boletos,
        "price" : ticket_price,
        "start_date" : fecha_inicio+" 00:00:00",
        "end_date" : fecha_fin+" 00:00:00"
      },
    }).done(function(data){
      data = JSON.parse(data);
      if(data > 0){
        var c = "";
        c += "<p>Rifa creada exitosamente</p>";
        c += "<div class='botones_modal'>";
        c += "  <button class='boton_ok_modal llevar_rifa' id='" + data + "'>Ok</button>";
        c += "</div>";
        new Modal().modal_generico_(c);
        eventos();
      }
      else{
        alert("Ocurrio un error inesperado por favor intenta de nuevo");
      }
    });
  }

}
