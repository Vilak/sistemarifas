function Activas(){
  var url = "";

  this.setUrl = function(base_url){
    url = base_url;
  }

  this.eventos_ = function(){
    eventos();
  }

  var eventos = function(){
    $(".boton_ver_activa").off();

    $(".boton_ver_activa").click(function(){
      var rifa_id = $(this).attr("id");
      localStorage.setItem("rifa_id",rifa_id);
      window.location.href = url + "index.php/Rifas";
    });
  }

  this.llenar_activas_ = function(){
    llenar_activas();
  }

  var llenar_activas = function(){
    $.ajax({
			method : 'POST',
			async : true,
			url : url + 'index.php/welcome/traer_rifas_activas',
			data: {},
		}).done(function(data){
			data = JSON.parse(data);
      if(data != ""){
        var c = "";
			  for (var i = 0; i < data.length; i++) {
          var fecha_fin = data[i].fecha_fin.split(" ");
				  c += "<tr>";
				  c += "	<td>" + data[i].nombre_rifa + "</td>";
				  c += "	<td>" + fecha_fin[0] +"</td>";
          c += "	<td>" + data[i].cantidad_boletos + "</td>";
          c += "	<td>" + data[i].max_ticket + "</td>";
          c += "	<td>" + data[i].precio_boleto + "</td>";
          c += "	<td><button class='boton_ver_activa' id='" + data[i].idrifa + "'>Ver</button></td>";
          c += "</tr>";
        }
			  $(".cuerpo_tabla_activas").html(c);
			  eventos();
      }
      else{
        alert("No hay rifas activas");
        $(".container_tabla").html("<p style='text-align:center;'>Ninguna rifa activa</p>");
      }
		});
		$(".rifas_ongoing").prop("disabled",true);
  }
}
