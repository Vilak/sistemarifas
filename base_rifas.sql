-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-11-2018 a las 22:31:16
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `base_rifas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleto`
--

CREATE TABLE `boleto` (
  `idboleto` int(11) NOT NULL,
  `num_boleto_rifa` int(11) NOT NULL,
  `pagado` tinyint(1) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `rifa_id` int(11) NOT NULL,
  `vendedor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `boleto`
--

INSERT INTO `boleto` (`idboleto`, `num_boleto_rifa`, `pagado`, `cliente_id`, `rifa_id`, `vendedor_id`) VALUES
(1, 1, 0, 1, 1, 1),
(3, 5, 1, 1, 1, 2),
(4, 1, 0, 1, 2, 2),
(5, 7, 0, 1, 2, 1),
(6, 6, 0, 1, 1, 2),
(7, 7, 0, 1, 1, 1),
(8, 8, 0, 1, 1, 2),
(9, 8, 0, 1, 1, 2),
(10, 9, 0, 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellido_paterno` varchar(45) NOT NULL,
  `apellido_materno` varchar(45) NOT NULL,
  `correo_electronico` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nombres`, `apellido_paterno`, `apellido_materno`, `correo_electronico`, `telefono`) VALUES
(1, 'nombre', 'paterno', 'materno', 'asd@asd.asd', '699696969');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `idpagos` int(11) NOT NULL,
  `fecha_pago` datetime NOT NULL,
  `cantidad_pago` varchar(45) NOT NULL,
  `boleto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`idpagos`, `fecha_pago`, `cantidad_pago`, `boleto_id`) VALUES
(1, '2018-09-05 00:00:00', '3', 3),
(2, '2018-11-01 00:00:00', '4', 3),
(3, '2018-11-25 18:13:19', '12', 3),
(4, '2018-11-25 18:40:48', '12', 1),
(9, '2018-11-25 19:07:22', '31', 3),
(10, '2018-11-25 19:08:26', '31', 3),
(11, '2018-11-25 19:08:58', '31', 3),
(12, '2018-11-25 19:09:21', '31', 3),
(13, '2018-11-25 19:14:30', '2', 1),
(14, '2018-11-25 19:15:11', '1', 1),
(15, '2018-11-25 19:20:35', '1', 1),
(16, '2018-11-25 19:20:57', '1', 1),
(17, '2018-11-25 19:22:03', '1', 1),
(18, '2018-11-25 21:53:51', '12', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rifa`
--

CREATE TABLE `rifa` (
  `idrifa` int(11) NOT NULL,
  `nombre_rifa` varchar(100) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `precio_boleto` float NOT NULL,
  `cantidad_boletos` int(11) NOT NULL,
  `ganador_id` int(11) DEFAULT NULL,
  `status_rifa` enum('Activa','Cancelada','Terminada') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rifa`
--

INSERT INTO `rifa` (`idrifa`, `nombre_rifa`, `fecha_inicio`, `fecha_fin`, `precio_boleto`, `cantidad_boletos`, `ganador_id`, `status_rifa`) VALUES
(1, 'rifa1', '2018-11-06 00:00:00', '2018-11-30 00:00:00', 50, 100, 1, 'Activa'),
(2, 'rifa2', '2018-11-01 00:00:00', '2018-11-30 00:00:00', 150, 50, NULL, 'Activa'),
(3, 'asd', '2018-11-01 00:00:00', '2018-11-30 00:00:00', 0, 12, NULL, 'Activa'),
(4, 'qweqweqwe', '2018-11-01 00:00:00', '2018-11-30 00:00:00', 0, 4, NULL, 'Activa'),
(5, 'qweqweqwe', '2018-11-01 00:00:00', '2018-11-30 00:00:00', 0, 2, NULL, 'Activa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedor`
--

CREATE TABLE `vendedor` (
  `idvendedor` int(11) NOT NULL,
  `nombre_vendedor` varchar(50) NOT NULL,
  `ap_vendedor` varchar(20) NOT NULL,
  `am_vendedor` varchar(20) NOT NULL,
  `telefono_vendedor` varchar(20) DEFAULT NULL,
  `email_vendedor` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `vendedor`
--

INSERT INTO `vendedor` (`idvendedor`, `nombre_vendedor`, `ap_vendedor`, `am_vendedor`, `telefono_vendedor`, `email_vendedor`) VALUES
(1, 'fulanin', 'ap peterno', 'ap materno', '8111111111', 'asd@asd.asd'),
(2, 'mister', 'faker', 'sktskt', '6969696969', 'qwer@qwer.qwer');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `boleto`
--
ALTER TABLE `boleto`
  ADD PRIMARY KEY (`idboleto`),
  ADD UNIQUE KEY `idboleto_UNIQUE` (`idboleto`),
  ADD KEY `cliente_id_idx` (`cliente_id`),
  ADD KEY `rida_id_idx` (`rifa_id`),
  ADD KEY `vendedor_id_idx` (`vendedor_id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`),
  ADD UNIQUE KEY `idcliente_UNIQUE` (`idcliente`),
  ADD UNIQUE KEY `correo_electronico_UNIQUE` (`correo_electronico`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`idpagos`),
  ADD KEY `boleto_id_idx` (`boleto_id`);

--
-- Indices de la tabla `rifa`
--
ALTER TABLE `rifa`
  ADD PRIMARY KEY (`idrifa`),
  ADD UNIQUE KEY `idrifa_UNIQUE` (`idrifa`),
  ADD KEY `winner_id_idx` (`ganador_id`);

--
-- Indices de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  ADD PRIMARY KEY (`idvendedor`),
  ADD UNIQUE KEY `idvendedor_UNIQUE` (`idvendedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `boleto`
--
ALTER TABLE `boleto`
  MODIFY `idboleto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `idpagos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `rifa`
--
ALTER TABLE `rifa`
  MODIFY `idrifa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  MODIFY `idvendedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `boleto`
--
ALTER TABLE `boleto`
  ADD CONSTRAINT `cliente_id` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `rida_id` FOREIGN KEY (`rifa_id`) REFERENCES `rifa` (`idrifa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `vendedor_id` FOREIGN KEY (`vendedor_id`) REFERENCES `vendedor` (`idvendedor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `boleto_id` FOREIGN KEY (`boleto_id`) REFERENCES `boleto` (`idboleto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rifa`
--
ALTER TABLE `rifa`
  ADD CONSTRAINT `winner_id` FOREIGN KEY (`ganador_id`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
