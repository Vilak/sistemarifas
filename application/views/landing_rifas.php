	<link rel="stylesheet" href="<?php echo base_url("assets/css/activas.css")?>">
	<main class="contenedor_principal">
		<p class="texto_header_tablas">Rifas Activas</p>
		<div class="container_tabla">
			<table class="tabla_rifas_actuales">
				<tr>
					<th>Nombre de La Rifa</th>
					<th>Fecha de Término</th>
					<th>Cantidad de Boletos</th>
					<th>Boletos Vendidos</th>
					<th>Precio por Boleto</th>
				</tr>
				<tbody class="cuerpo_tabla_activas">

				</tbody>
			</table>
		</div>
	</main>
</body>
<script src="<?php echo base_url('assets/js/activas.js')?>"></script>
<script>
var url = "http://127.0.0.1/SistemaRifas/";
	$(document).ready(function(){
		var activas = new Activas();
		activas.setUrl(url);
		activas.llenar_activas_();
	});
</script>
</html>
