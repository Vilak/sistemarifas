<link rel="stylesheet" href="<?php echo base_url("assets/css/rifas.css")?>">
<main class="contenedor_principal">
  <p class="nombre_rifa"></p>
  <div class="container_tabla_info_rifa">
    <p class="nobre_rifa"></p>
    <table class="tabla_info_rifa">
      <tr>
        <th>Nombre de la Rifa</th>
        <th>Fecha de Inicio</th>
        <th>Fecha Fin</th>
        <th>Precio por Boleto</th>
        <th>Cantidad de Boletos</th>
        <th>Cantidad de Boletos Vendidos</th>
        <th>Cantidad Recaudada</th>
      </tr>
      <tbody class="tabla_rifa">

      </tbody>
    </table>
    <div class="" style="text-align:center;margin-top:7px;">
      <button class="agregar_boleto">Nuevo boleto</button>
    </div>
    <table class="tabla_info_boletos" style="margin-top:2%;">
      <tr>
        <th>Vendedor</th>
        <th>Numero de Boleto</th>
        <th>Nombre del Comprador</th>
        <th>Telefono</th>
        <th>Cantidad Abonada</th>
        <th>Pagado</th>
      </tr>
      <tbody class="tabla_boletos">

      </tbody>
    </table>
  </div>
</main>
<script src="<?php echo base_url("assets/js/rifa.js")?>"></script>
<script>
  var url = "http://127.0.0.1/SistemaRifas/";
  $(document).ready(function(){
    var rifa_id = localStorage.getItem("rifa_id");
    var rifa = new Rifa();
    rifa.setUrl(url);
    rifa.cargar_rifa_(rifa_id);
  });
</script>
