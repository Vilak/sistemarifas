<div class="container_modal">
  <div class="modal_contenido">
    <div class="modal_texto">
      <div>
        <p id="modal_titulo"></p>
      </div>
      <div>
        <p id="modal_texto"></p>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url('assets/js/main.js')?>" charset="utf-8"></script>
<script src="<?php echo base_url("assets/js/modal_js.js")?>"></script>
<script>

  $(document).ready(function(){
  	var main = new Main();
	  main.setUrl(url);
    main.eventos_();
  });

</script>
