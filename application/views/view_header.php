<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>RiTfa</title>
	<link rel="stylesheet" href="<?php echo base_url("assets/css/barlow.css")?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/raleway.css")?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/css/all.css")?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/main.css")?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/jquery-ui.min.css")?>">
</head>
<script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>"></script>
<header>
	<button class="rifas_anterior">Rifas Anteriores</button>
	<button class="rifas_ongoing">Rifas Acutales</button>
	<button class="nueva_rifa">Nueva Rifa</button>
</header>
<div class="settings">
	<i class="fas fa-users floating_settings"></i>
</div>
<body>
