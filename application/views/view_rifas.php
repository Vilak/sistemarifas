<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Rifas</title>
	<link rel="stylesheet" href="<?php echo base_url('/assets/css/rifas.css') ?>">
</head>
<body>

	<h1>Rifas Pasadas</h1>

	<table>
		<thead>
			<tr>
				<th>Nombre de la Rifa</th>
				<th>Fecha de Inicio</th>
				<th>Fecha de Fin</th>
			</tr>
		</thead>
		<tbody class="cuerpo_rifas">

		</tbody>
	</table>

<button class="clase">Alert</button>

</body>
<script src="<?php echo base_url('/assets/js/landing.js')?>"></script>
<script>
	$(document).ready(function(){
		var url = "<?php echo base_url()?>";
		var coso = new Cosita();
		coso.eventos_();
		coso.setUrl_(url);
	});
</script>
</html>
