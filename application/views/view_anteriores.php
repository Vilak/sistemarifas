<link rel="stylesheet" href="<?php echo base_url("assets/css/anteriores.css")?>">
<main class="contenedor_principal">
  <p class="texto_header_tablas">Rifas Anteriores</p>
  <div class="container_tabla">
    <table class="tabla_anteriores">
      <tr>
        <th>Nombre de La Rifa</th>
        <th>Fecha de Inicio</th>
        <th>Fecha de Término</th>
        <th>Cantidad de Boletos</th>
        <th>Boletos Vendidos</th>
        <th>Total recaudado</th>
        <th>Ganador</th>
      </tr>
      <tbody class="tabla_rifas_ant">

      </tbody>
    </table>
  </div>
</main>
<script src="<?php echo base_url("assets/js/anteriores.js")?>"></script>
<script>
  var url = "http://127.0.0.1/SistemaRifas/";
  $(document).ready(function(){
  	var anterior = new Anteriores();
  	anterior.setUrl(url);
  	anterior.llenar_anteriores_();
  });
  $(".rifas_anterior").prop("disabled",true);
</script>
