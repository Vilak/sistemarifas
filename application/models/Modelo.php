<?php
class Modelo extends CI_Model{

  public function __construct() {
        parent::__construct();
        $this->load->database();
    }

  // hacer un query que devuelve filas
  function query($query){
    return  $this->db->query($query)->result();
  }

  // hacer un query que no devuelve filas (insert, update)
  function query_no_return($query)
	{
		$this->db->query($query);
	}

}
?>
