<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rifas extends CI_Controller{

	public $CI = NULL;

	function __construct(){
    parent::__construct();
		$this->load->database();
    $this->load->model('Modelo');
    $this->load->helper('url');
		$this->CI = & get_instance();
	}

	public function index(){
		$this->load->view('view_header');
		$this->load->view('view_activas');
		$this->load->view('view_footer');
	}

	public function traer_rifa(){
		$rifa_id = $this->input->POST("rifa_id");
		$query_rifa = "SELECT * FROM rifa WHERE idrifa = " . $rifa_id . "";
		$call_rifa = $this->Modelo->query($query_rifa);

		$query_boletos = "SELECT * FROM boleto JOIN vendedor ON (boleto.vendedor_id = vendedor.idvendedor) JOIN cliente ON (cliente.idcliente = boleto.cliente_id) WHERE rifa_id = " . $rifa_id . "";
		$call_boletos = $this->Modelo->query($query_boletos);
		for($i=0; $i < count($call_boletos) ; $i++){
			$query_pagos = "SELECT * FROM pagos JOIN boleto ON (boleto.idboleto = pagos.boleto_id) WHERE boleto_id = " . $call_boletos[$i]->idboleto . "";
			$call_pagos = $this->Modelo->query($query_pagos);
			$pago_boleto = 0;
			if(count($call_pagos) > 0){
				for ($j=0; $j < count($call_pagos) ; $j++) {
					$pago_boleto = $pago_boleto + $call_pagos[$j]->cantidad_pago;
				}
			}
			$call_boletos[$i]->cantidad_pagada = $pago_boleto;
		}
		$total = 0;
		for ($i=0; $i < count($call_boletos); $i++) {
			$total = $total + $call_boletos[$i]->cantidad_pagada;
		}



		$call_rifa[0]->total_recaudado = $total;

		$call_rifa[0]->boletos_vendidos = count($call_boletos);
		$resultado["info_rifa"] = $call_rifa;
		$resultado["info_boletos"] = $call_boletos;
		echo json_encode($resultado);
	}

	public function traer_info_boleto(){
		$boleto_id = $this->input->POST("boleto_id");
		$query_boleto = "SELECT * FROM boleto JOIN cliente ON (cliente.idcliente = boleto.cliente_id) JOIN vendedor ON (vendedor.idvendedor = boleto.vendedor_id) JOIN rifa ON (rifa.idrifa = boleto.rifa_id) WHERE idboleto = " . $boleto_id . "";
		$call_boleto = $this->Modelo->query($query_boleto);
		$query_pagos = "SELECT * FROM pagos WHERE boleto_id = " . $boleto_id . "";
		$call_pagos = $this->Modelo->query($query_pagos);
		$resultado["boleto"] = $call_boleto;
		$resultado["pagos"] = $call_pagos;
		echo json_encode($resultado);
	}

	public function guardar_pago_boleto(){
		$boleto_id = $this->input->POST("ticket_id");
		$cantidad = $this->input->POST("cant_pago");
		$fecha = date("Y-m-d H:i:s");
		$resultado["pagado"] = false;
		$query_pago = "INSERT INTO pagos (fecha_pago,cantidad_pago,boleto_id) VALUES ('" . $fecha . "',$cantidad,$boleto_id)";
		$this->Modelo->query_no_return($query_pago);
		$query_check_pagado = "SELECT * FROM pagos WHERE boleto_id = " . $boleto_id . "";
		$call_check_pagado = $this->Modelo->query($query_check_pagado);
		$call_rifaid = $this->Modelo->query("SELECT * FROM boleto WHERE idboleto = " . $boleto_id . "");
		$query_precio = "SELECT precio_boleto FROM rifa WHERE idrifa = " . $call_rifaid[0]->rifa_id . "";
		$call_precio = $this->Modelo->query($query_precio);
		if(count($call_check_pagado) > 0){
			$sum_payments = 0;
			for($i=0; $i < count($call_check_pagado); $i++){
				$sum_payments = $sum_payments + $call_check_pagado[$i]->cantidad_pago;
			}
			if($sum_payments >= $call_precio[0]->precio_boleto){
				$query_cambio_pago = "UPDATE boleto SET pagado = 1 WHERE idboleto = " . $boleto_id . "";
				$this->Modelo->query_no_return($query_cambio_pago);
				$resultado["pagado"] = true;
			}
			else{
				$resultado["restante"] = $call_precio[0]->precio_boleto - $sum_payments;
			}
		}
		echo json_encode($resultado);

	}

	public function traer_info_compra_boleto(){
		$id_rifa = $this->input->POST("rifa_id");
		$query_clientes = "SELECT * FROM cliente";
		$resultado["clientes"] = $this->Modelo->query($query_clientes);
		$query_vendedores = "SELECT * FROM vendedor";
		$resultado["vendedores"] = $this->Modelo->query($query_vendedores);
		$query_boletos = "SELECT * FROM boleto WHERE rifa_id = " . $id_rifa . " ORDER BY num_boleto_rifa DESC";
		$call_boletos = $this->Modelo->query($query_boletos);
		if(count($call_boletos) > 0){
			$resultado["boletos"] = $call_boletos;
		}
		else{
			$resultado["boletos"] = 0;
		}
		$query_rifa = "SELECT * FROM rifa WHERE idrifa = " . $id_rifa . "";
		$resultado["rifa"] = $this->Modelo->query($query_rifa);
		echo json_encode($resultado);
	}

	public function guarda_boleto(){
		$boleto_num = $this->input->POST("boleto_num");
		$rifa_id = $this->input->POST("rifa_id");
		$comprador = $this->input->POST("comprador");
		$vendedor = $this->input->POST("vendedor");
		$payment = $this->input->POST("payment");
		$payment = (float)$payment;
		echo var_dump($payment);
		$query_boleto = "INSERT INTO boleto (num_boleto_rifa,cliente_id,rifa_id,vendedor_id) VALUES ($boleto_num,$comprador,$rifa_id,$vendedor)";
		$call_boleto = $this->Modelo->query_no_return($query_boleto);
		if($payment > 0){
			echo var_dump("algo");
			$fecha = date("Y-m-d H:i:s");
			$query_pago = "INSERT INTO pagos (fecha_pago,cantidad_pago,boleto_id) VALUES ('" . $fecha . "','" . $payment . "','" . $boleto_num . "')";
			$call_pago = $this->Modelo->query_no_return($query_pago);
		}
	}
}
