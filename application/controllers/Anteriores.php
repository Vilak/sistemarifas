<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anteriores extends CI_Controller{

  public $CI = NULL;

  function __construct(){
    	parent::__construct();
		$this->load->database();
    	$this->load->model('Modelo');
    	$this->load->helper('url');
		$this->CI = & get_instance();
	}

  public function index(){
    $this->load->view('view_header');
		$this->load->view('view_anteriores');
    $this->load->view('view_footer');
	}

  public function traer_rifas_anteriores(){
    $query_anteriores = "SELECT * FROM rifa WHERE status_rifa = 'Terminada'";
    $call_anteriores = $this->Modelo->query($query_anteriores);
    if(count($call_anteriores) > 0){
      for ($i=0; $i < count($call_anteriores) ; $i++) {
        $id_rifa = $call_anteriores[$i]->idrifa;
        $query_max_ticket = "SELECT MAX(num_boleto_rifa) AS tickets_tot FROM boleto WHERE rifa_id = " . $id_rifa . "";
        $call_max_ticket = $this->Modelo->query($query_max_ticket);
        $call_anteriores[$i]->tickets_tot = $call_max_ticket[0]->tickets_tot;

        if($call_anteriores[$i]->ganador_id != ""){
          $query_winner = "SELECT * FROM cliente WHERE idcliente = " . $call_anteriores[$i]->ganador_id . "";
          $call_winner = $this->Modelo->query($query_winner);
          $call_anteriores[$i]->ganador = $call_winner[0]->nombres;
        }
        else{
          $call_anteriores[$i]->ganador = false;
        }

        $query_total_amount = "SELECT * FROM boleto JOIN pagos ON (boleto.idboleto = pagos.boleto_id) WHERE rifa_id = " . $id_rifa . "";
        $call_total = $this->Modelo->query($query_total_amount);

        $total = 0;
        for($j=0; $j < count($call_total) ; $j++){
          $total = $total + floatval($call_total[$j]->cantidad_pago);
        }

        $call_anteriores[$i]->recaudado = $total;
      }
    }
    echo json_encode($call_anteriores);
  }
}
?>
