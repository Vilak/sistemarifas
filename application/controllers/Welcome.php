<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public $CI = NULL;

	function __construct() {
    parent::__construct();
		$this->load->database();
    $this->load->model('Modelo');
    $this->load->helper('url');
		$this->CI = & get_instance();
	}

	public function index(){
		$this->load->view('view_header');
		$this->load->view('landing_rifas');
		$this->load->view('view_footer');
	}

	public function traer_rifas_activas(){
		$fecha_hoy = date('Y-m-d H:i:s');
		$query_activas = "SELECT * FROM rifa WHERE fecha_fin > '" . $fecha_hoy . "' AND fecha_inicio <= '" . $fecha_hoy . "' AND status_rifa = 'Activa'";
		$call_activas = $this->Modelo->query($query_activas);
		for($i=0 ; $i < count($call_activas) ; $i++){
			$id_rifa = $call_activas[$i]->idrifa;
			$query_max_ticket = "SELECT MAX(num_boleto_rifa) AS max_ticket FROM boleto WHERE rifa_id = " . $id_rifa . "";
			$call_max_ticket = $this->Modelo->query($query_max_ticket);
			if($call_max_ticket[0]->max_ticket != NULL){
				$call_activas[$i]->max_ticket = $call_max_ticket[0]->max_ticket;
			}
			else{
				$call_activas[$i]->max_ticket = 0;
			}
		}
		echo json_encode($call_activas);
	}

	public function guardar_rifa(){
		$rifa_id = -1;
		$nombre_rifa = $this->input->POST("nombre");
		$cant_boletos = $this->input->POST("tickets");
		$precio_boletos = $this->input->POST("price");
		$fecha_inicio = $this->input->POST("start_date");
		$fecha_fin = $this->input->POST("end_date");
		$query_nueva = "INSERT INTO rifa (nombre_rifa,fecha_inicio,fecha_fin,precio_boleto,cantidad_boletos,status_rifa) VALUES ('" . $nombre_rifa . "','" . $fecha_inicio . "','" . $fecha_fin . "','" . $precio_boletos . "','" . $cant_boletos . "', 'Activa')";
		$this->Modelo->query_no_return($query_nueva);
		$rifa_id = $this->db->insert_id();
		echo json_encode($rifa_id);
	}

	public function guarda_usuario(){
		$tipo = $this->input->POST("tipo");
		$nombres = $this->input->POST("nombres");
		$ap_p = $this->input->POST("ap_p");
		$ap_m = $this->input->POST("ap_m");
		$email = $this->input->POST("email");
		$phone = $this->input->POST("phone");
		if($tipo == 'cliente'){
			$query_user = "INSERT INTO cliente (nombres,apellido_paterno,apellido_materno,correo_electronico,telefono) VALUES ('$nombres','$ap_p','$ap_m','$email','$phone')";
		}
		else{
			$query_user = "INSERT INTO vendedor (nombre_vendedor,ap_vendedor,am_vendedor,email_vendedor,telefono_vendedor) VALUES ('$nombres','$ap_p','$ap_m','$email','$phone')";
		}
		$this->Modelo->query_no_return($query_user);
		echo json_encode(true);
	}
}
